# Example SFML application with older style OpenGL

This is a small example of immediate mode OpenGL, which is the old way of of doing 3D graphics.
This was simpler for programmers, but newer OpenGL is more flexible and has improved performance.

## Prerequisites
The C++ IDE [juCi++](https://github.com/cppit/jucipp) should be installed.

## Installing dependencies

### Debian based distributions
`sudo apt-get install libsfml-dev libglm-dev`

### Arch Linux based distributions
`sudo pacman -S sfml glm`

### OS X
`brew install sfml glm`

## Compiling and running
```sh
git clone https://gitlab.com/ntnu-tdat3023/sfml-old-opengl-example
juci sfml-old-opengl-example
```

Choose Compile and Run in the Project menu.
